# Contribution Records

This is the new Contribution Records system used by the new www.drupal.org site.

The way to store contribution records and credits will change after the issue GitLab migration and this new system will be the one storing and serving all the information about credits for individuals and organizations.

This module relies on the new www.drupal.org site configuration and assumes that certain content types, fields, views... already exist.

## Connect to Drupal 7 www.drupal.org

The connection is made via the `drupalorg` module and by setting a token that will allow both systems to communicate.
- The headers of the request need to include `Drupalorg-Webhook-Token` for the write operations.
- This token is stored under `drupalorg.settings` and then `token`.

## Import contribution records

The process is described in the [main issue](https://www.drupal.org/project/drupalorg/issues/3327584).

Drupal 7 will send raw information to this system. The contribution record can be created on-the-go or queued (recommended).
If the request is queued, then the new contribution records will be created running the queue like this:
`drush queue:run contribution_records_import_queue_worker`

## Sources of credit

The currently allowed sources of credit information are [Drupal.org](https://www.drupal.org) and [Drupal GitLab Issues](https://git.drupalcode.org).

The system will read all the participants, the status of the issue and will allow maintainers to grant credit to those participants.

## Useful for development

Delete all imported contribution records.
`drush entity:delete node --bundle=contribution_record`
