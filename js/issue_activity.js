(function (Drupal, drupalSettings) {
  /**
   * Process the activity response and updates the markup for each user.
   */
  function renderActivity(response) {
    if (response.status === 'success') {
      let contributorsListed = document.querySelectorAll('.contributor-username');
      contributorsListed.forEach((contributor) => {
        const username = contributor.innerText.trim();
        const activity = response.data.activity[username];
        if (activity !== undefined) {
          if (activity.comments === undefined) {
            activity.comments = 0;
          }
          if (activity.reactions === undefined) {
            activity.reactions = [];
          }
          if (activity.files === undefined) {
            activity.files = 0;
          }

          let userWrapper = contributor.parentNode;
          let activityText = [];
          if (activity.comments) {
            activityText.push(Drupal.formatPlural(activity.comments, '1 comment', '@comments comments', {
              '@comments': activity.comments
            }));
          }
          if (activity.files) {
            activityText.push(Drupal.formatPlural(activity.files, '1 file', '@files files', {
              '@files': activity.files
            }));
          }
          const reactionsLength = activity.reactions.length;
          if (reactionsLength) {
            let reactionsText = Drupal.formatPlural(reactionsLength, 'Reaction given: ', 'Reactions given: ') + activity.reactions.slice(0, 5).join(' ');
            if (reactionsLength > 5) {
              reactionsText += ' …';
            }
            activityText.push(reactionsText);
          }

          if (activityText.length) {
            activityText = activityText.join('<br>');
            const previousActivity = userWrapper.querySelector('.issueActivity');
            if (previousActivity) {
              userWrapper.removeChild(previousActivity);
            }
            userWrapper.innerHTML = userWrapper.innerHTML + '<span class="issueActivity description">' + activityText + '</span>';
          }
        }
      });
    }
  }

  /**
   * Replace plain links with meta info like title, status, etc.
   */
  function addMetaInfoLink(linkMeta) {
    let linkElement = document.querySelector('a.related-link[href="' + linkMeta['link'] + '"]');
    if (linkElement !== undefined) {
      linkElement.setAttribute('target', '_blank');
      linkElement.classList.add(linkMeta['type']);
      if (linkMeta['type'] === 'merge-request-gitlab') {
        const iid = linkMeta['link'].split('/').pop();
        linkElement.textContent = Drupal.t('MR #@iid', {'@iid': iid});
        if (linkMeta['status'] === 'closed') {
          linkElement.insertAdjacentHTML('afterend', ' <small>' + Drupal.t('CLOSED') + '</small>');
        }
        linkElement.setAttribute('title', linkMeta['title']);
        // linkElement.insertAdjacentHTML('afterend', '<span>' + linkMeta['title'] + '</span>');
      }
      else {
        linkElement.textContent = linkMeta['title'];
        if (linkMeta['status'] === 'closed') {
          linkElement.insertAdjacentHTML('afterend', ' <small>' + Drupal.t('CLOSED') + '</small>');
        }
      }
    }
  }

  /**
   * Sets a loading message and returns its id.
   */
  function setLoadingMessage(message) {
    const randomId = Math.floor(Math.random() * 100);
    const loadingMessageWrapperSelector = drupalSettings.contribution_records.loading_wrapper_selector;
    if (loadingMessageWrapperSelector) {
      let loadingMessage = document.createElement('div');
      loadingMessage.setAttribute('id', 'loading_' + randomId);
      loadingMessage.classList.add('loading_activity');
      loadingMessage.innerHTML = message;
      document.querySelector(loadingMessageWrapperSelector).prepend(loadingMessage);
    }

    return 'loading_' + randomId;
  }

  /**
   * Cleans a loading message.
   */
  function cleanLoadingMessage(loadingId) {
    document.getElementById(loadingId).remove();
  }

  /**
   * Fetches activity from a URL and renders the results.
   */
  function fetchActivity(link) {
    // console.log(link);
    let urlParams = new URLSearchParams();
    urlParams.append('source_link', link);
    urlParams.append('maintainer', drupalSettings.contribution_records.maintainer ? 1 : 0);
    return fetch(drupalSettings.path.baseUrl + 'contribution-record-source-activity?' + urlParams)
      .then(response => response.json());
  }

  Drupal.behaviors.issueActivity = {
    attach: async function (context, settings) {
      const contributorsWrapper = once('contributors', 'body', context);
      if (contributorsWrapper.length) {
        let combinedData = {};
        const loadingId = setLoadingMessage(Drupal.t('Loading per-user activity information...'));
        fetchActivity(drupalSettings.contribution_records.source_link)
          .then(result => {
            if (result.status === 'success') {
              combinedData = result;
              renderActivity(combinedData);
            }
            cleanLoadingMessage(loadingId);
          })
          .then(() => {
            let relatedLinks = document.querySelectorAll('.related-link');
            if (relatedLinks.length) {
              const relatedLoadingId = setLoadingMessage(Drupal.t('Loading per-user activity information for related links'));
              Promise.all(Array.from(relatedLinks).map(related_link =>
                fetchActivity(related_link.href)
              ))
              .then(resultsArray => {
                resultsArray.forEach(result => {
                  if (result.status === 'success') {
                    addMetaInfoLink(result.data.meta);

                    // Merge / add data to the combinedData array.
                    Object.entries(result.data.activity).forEach(([key, value]) => {
                      if (combinedData.data.activity[key] === undefined) {
                        combinedData.data.activity[key] = value;
                      }
                      else {
                        combinedData.data.activity[key].comments += value.comments;
                        combinedData.data.activity[key].files += value.files;
                        if (value.reactions.length) {
                          combinedData.data.activity[key].reactions = combinedData.data.activity[key].reactions.concat(value.reactions);
                        }
                      }
                    });
                  }
                });
                renderActivity(combinedData);
                cleanLoadingMessage(relatedLoadingId);
              })
              .catch(error => {
                // console.log(error);
                cleanLoadingMessage(relatedLoadingId);
              });
            }
          })
          .catch(error => {
            // console.log(error);
            cleanLoadingMessage(loadingId);
          });
      }
    }
  }
} (Drupal, drupalSettings));
