(function (Drupal, drupalSettings) {
  Drupal.behaviors.autosubmit = {
    attach: function (context, settings) {
      const contributionRecordForm = document.forms['node-contribution-record-form'];
      const sourceLink = document.getElementsByName('field_source_link[0][uri]');

      if (sourceLink.length && contributionRecordForm.length) {
        if (
          !contributionRecordForm.classList.contains('submitted') &&
          !sourceLink[0].classList.contains('error') &&
          sourceLink[0].value.length > 0
        ) {
          contributionRecordForm.classList.add('submitted');
          contributionRecordForm.submit();
        }
      }
    }
  }
} (Drupal, drupalSettings));
