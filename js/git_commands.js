(function (Drupal) {
  // Build the git command.
  function getGitCommand(text) {
    return "git commit -m '" + text + "'";
  }

  function setGitMessage() {
    let issueTitle = document.getElementsByTagName('h1');
    const issueUrl = document.querySelector('.source a').textContent;
    const gitCommand = document.getElementById('git-command');
    const availableUsers = document.querySelectorAll('.credited');
    const taskTypeInput = document.getElementById('task-type');

    // Extract issue number from URL.
    let issueNumber = '';
    const extractedId = parseInt(issueUrl.split('/').pop());
    if (extractedId > 0) {
      issueNumber = '[#' + extractedId + '] ';
    }

    // Populate (or hide) select with possible authors for the commit.
    let byCredited = '';
    if (availableUsers.length > 0) {
      availableUsers.forEach(function(user) {
        const username = user.querySelector('.contributor-username').textContent;
        const email = user.querySelector('.git-author').textContent;
        // @todo What makes a reviewer and author?
        byCredited += Drupal.t("Authored-by: !author\n", {'!author': (email.length ? email : username)});
      });
    }

    // Set the initial title.
    if (issueTitle.length) {
      const taskType = taskTypeInput.value;
      // https://github.com/pvdlg/conventional-changelog-metahub#commit-types
      issueTitle = issueNumber + taskType + ': ' + issueTitle[0].innerText + (byCredited.length ? "\n\n" + byCredited : '');
      gitCommand.innerText = getGitCommand(issueTitle);
    }
  }

  let initialised = false;
  Drupal.behaviors.gitCommands = {
    attach: function (context) {
      if (!initialised && context === document) {
        initialised = true;
        const taskTypeInput = document.getElementById('task-type');
        let checkboxes = document.querySelectorAll('.quick-credit-table.form-checkbox');
        setGitMessage();
        taskTypeInput.addEventListener('change', function(event) {
          setGitMessage();
        });
        checkboxes.forEach((input) => {
          input.addEventListener('change', (event) => {
            const parentRow = event.target.closest('tr');
            if (event.target.checked) {
              parentRow.classList.remove('not-credited');
              parentRow.classList.add('credited');
            }
            else {
              parentRow.classList.remove('credited');
              parentRow.classList.add('not-credited');
            }
            setGitMessage();
          });
        });
      }
    }
  };
})(Drupal);
