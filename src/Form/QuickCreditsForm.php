<?php

namespace Drupal\contribution_records\Form;

use Drupal\contribution_records\SourceLink;
use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Settings form for Quick Credits.
 */
class QuickCreditsForm extends FormBase {

  /**
   * The class resolver.
   *
   * @var \Drupal\Core\DependencyInjection\ClassResolverInterface
   */
  protected ClassResolverInterface $classResolver;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Constructs a MenuLinkEditForm object.
   *
   * @param \Drupal\Core\DependencyInjection\ClassResolverInterface $class_resolver
   *   The class resolver.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(ClassResolverInterface $class_resolver, EntityTypeManagerInterface $entity_type_manager) {
    $this->classResolver = $class_resolver;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('class_resolver'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'contribution_records_quick_credits';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ?NodeInterface $node = NULL) {
    if ($node->getType() !== 'contribution_record') {
      return $form;
    }

    $form_state->set('contribution_record', $node);

    // Build checkboxes.
    $contributors_paragraphs = $node->get('field_contributors')->referencedEntities();
    if (!empty($contributors_paragraphs)) {
      $table = [];
      $preselected = [];
      foreach ($contributors_paragraphs as $contributor) {
        $contributor_credit = !empty($contributor->get('field_credit_this_contributor')->getValue()) && $contributor->get('field_credit_this_contributor')->getValue()[0]['value'];
        /** @var \Drupal\user\UserInterface $contributor_user */
        $contributor_user = $contributor->get('field_contributor_user')->entity;
        $contributor_username = $contributor_user->getAccountName();
        $attribute_orgs = !empty($contributor->get('field_contributor_attribute_orgs')->getValue()) && $contributor->get('field_contributor_attribute_orgs')->getValue()[0]['value'];
        $organizations = [];
        $customers = [];

        if ($attribute_orgs) {
          $contributor_organizations = $contributor->get('field_contributor_organisation')->referencedEntities();
          if (is_array($contributor_organizations)) {
            foreach ($contributor_organizations as $organization) {
              $organizations[] = $organization->label();
            }
          }

          $contributor_customers = $contributor->get('field_contributor_customer')->referencedEntities();
          if (is_array($contributor_customers)) {
            foreach ($contributor_customers as $organization) {
              $customers[] = $organization->label();
            }
          }
        }

        $git_author = !empty($contributor_user->get('field_git_username')->getValue()) ?
          $contributor_user->get('field_git_username')->getValue()[0]['value'] :
          $contributor_user->getAccountName();
        $git_author .= ' <' . $contributor_user->getEmail() . '>';
        $table[$contributor_user->id()] = [
          '#attributes' => [
            'class' => [$contributor_credit ? 'credited' : 'not-credited'],
          ],
          'title' => ['data' => ['#title' => t('Credit @user', ['@user' => $contributor_username])]],
          'user' => t('
            <span class="contributor-username">@contributor</span>
            <span class="visually-hidden git-author">@gitauthor</span>
            <span class="credit-organizations">@organization</span>
            <span class="credit-organizations">@customer</span>
          ', [
            '@contributor' => Link::fromTextAndUrl($contributor_username, $contributor_user->toUrl())->toString(),
            '@gitauthor' => $git_author,
            '@organization' => count($organizations) ? new TranslatableMarkup('<span>organization(s):</span> @organizations', [
              '@organizations' => implode(',', $organizations),
            ]) : '',
            '@customer' => count($customers) ? new TranslatableMarkup('<span>customer(s):</span> @customers', [
              '@customers' => implode(',', $customers),
            ]) : '',
          ]),
        ];
        if ($contributor_credit) {
          $preselected[$contributor_user->id()] = TRUE;
        }
      }

      $form['quick_credit_wrapper'] = [
        '#type' => 'fieldset',
        '#id' => 'quick-credit-wrapper',
        '#title' => t('Contributors'),
        '#description' => t('Assign credits easily using this table. Review and make sure that all granted credits are present and fair.'),
        '#description_display' => 'before',
      ];
      $form['quick_credit_wrapper']['quick_credit'] = [
        '#type' => 'tableselect',
        '#id' => 'quick-credit-table',
        '#header' => [
          'user' => t('Give credit to everyone'),
        ],
        '#options' => $table,
        '#default_value' => $preselected,
        '#attributes' => ['class' => ['quick-credit-table']],
        '#empty' => t('No users can be credited yet.'),
      ];
      /** @var \Drupal\contribution_records\SourceLink $source_link_instance */
      $source_link_instance = $this->classResolver->getInstanceFromDefinition(SourceLink::class);
      $source_link = $source_link_instance->setLink($node->get('field_source_link')->getValue()[0]['uri']);
      $form['#attached']['library'][] = 'contribution_records/issue_activity';
      $form['#attached']['drupalSettings']['contribution_records'] = [
        'source_link' => $source_link->getLink(),
        'maintainer' => TRUE,
        'loading_wrapper_selector' => '#quick-credit-wrapper .fieldset__wrapper',
      ];
    }

    // Credit others autocomplete.
    $form['credit_others'] = [
      '#type' => 'entity_autocomplete',
      '#title' => t('Credit others'),
      '#description' => t('Comma separated list of usernames. A granted credit for each person will be added. <br>Start typing the username and select it when the full username comes up.'),
      '#description_display' => 'before',
      '#target_type' => 'user',
      '#tags' => TRUE,
      '#selection_settings' => [
        'include_anonymous' => FALSE,
      ],
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => $this->t('Save'),
    ];

    // Add possibility of re-syncing contributors on demand and metadata.
    $form['actions']['sync_contributors'] = [
      '#type' => 'submit',
      '#button_type' => 'secondary',
      '#value' => t('Sync from source'),
      '#submit' => [[$this, 'submitSyncFromSource']],
      '#weight' => 1,
    ];

    $related_links = $source_link->getRelatedLinks();
    if (!empty($related_links)) {
      $form['actions']['import_from_related'] = [
        '#type' => 'submit',
        '#button_type' => 'secondary',
        '#weight' => 2,
        '#value' => $this->t('Sync from related'),
        '#submit' => [[$this, 'submitSyncFromRelated']],
      ];
    }

    return $form;
  }

  /**
   * Action callback.
   *
   * @param array $form
   *   Form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Submitted form.
   */
  protected function submitSyncFromRelated(array &$form, FormStateInterface $form_state) {
    $node = $form_state->get('contribution_record');
    _contribution_records_sync_from_related($node);
    $this->messenger()->addStatus($this->t('Contributors were synced from the related links'));
  }

  /**
   * Action callback.
   *
   * @param array $form
   *   Form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Submitted form.
   */
  protected function submitSyncFromSource(array &$form, FormStateInterface $form_state) {
    $node = $form_state->get('contribution_record');
    _contribution_records_sync_from_source($node);
    $this->messenger()->addStatus($this->t('Contributors were synced from the source link'));
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\node\NodeInterface $node */
    $node = $form_state->get('contribution_record');
    $contributors_user_ids = [];

    $quick_credits = array_keys(array_filter($form_state->getValue('quick_credit', [])));
    // Update credits.
    $contributors = $node->field_contributors->referencedEntities();
    foreach ($contributors as $contributor) {
      $user_id = $contributor->get('field_contributor_user')->entity->id();
      $credit = in_array($user_id, $quick_credits);
      $contributor->get('field_credit_this_contributor')->setValue($credit);
      $contributor->save();

      $contributors_user_ids[] = $user_id;
    }

    $credit_others = $form_state->getValue('credit_others');
    if (!empty($credit_others)) {
      foreach ($credit_others as $user) {
        if (!in_array($user['target_id'], $contributors_user_ids)) {
          $user_id = $user['target_id'];
          $paragraph = $this->entityTypeManager->getStorage('paragraph')->create([
            'type' => 'contributor',
            'field_contributor_user' => ['target_id' => $user_id],
            'field_credit_this_contributor' => TRUE,
          ]);
          $paragraph->save();
          $node->field_contributors->appendItem($paragraph);
          $contributors_user_ids[] = $user_id;
        }
      }
    }

    $this->messenger()->addMessage($this->t('Credit information updated.'));
    $node->save();
  }

}
