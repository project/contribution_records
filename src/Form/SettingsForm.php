<?php

namespace Drupal\contribution_records\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Settings form for Contribution Records.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'contribution_records_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['contribution_records.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('contribution_records.settings');
    $form['allow_dev_sources'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow dev sources.'),
      '#description' => $this->t('Allow fetching data from gitlab1 (non-production) Drupal and GitLab instances.'),
      '#default_value' => $config->get('allow_dev_sources'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('contribution_records.settings')
      ->set('allow_dev_sources', $form_state->getValue('allow_dev_sources'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
