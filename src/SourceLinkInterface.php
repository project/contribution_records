<?php

namespace Drupal\contribution_records;

/**
 * All Source classes should implement this.
 */
interface SourceLinkInterface {

  /**
   * Open status.
   *
   * @var string
   */
  public const STATUS_OPEN = 'open';

  /**
   * Open status.
   *
   * @var string
   */
  public const STATUS_CLOSED = 'closed';

  /**
   * Gets the title linked to the source.
   *
   * @return string
   *   Title linked to the source.
   */
  public function getTitle(): string;

  /**
   * Gets the project linked to the source.
   *
   * @return string|null
   *   Project composer_namespace.
   */
  public function getProject(): ?string;

  /**
   * Gets the current status of the issue.
   *
   * @return string
   *   Status of the issue: "open" or "closed".
   */
  public function getStatus(): string;

  /**
   * Returns the created timestamp.
   *
   * @return int
   *   Created timestamp.
   */
  public function getCreated(): int;

  /**
   * Returns the changed timestamp.
   *
   * @return int
   *   Changed timestamp.
   */
  public function getChanged(): int;

  /**
   * Returns the timestamp of the last time the status changed.
   *
   * @return int
   *   Last Status Change timestamp.
   */
  public function getLastStatusChanged(): int;

  /**
   * Whether the issue represented by the link is considered closed.
   *
   * For issues, this is a close-related state, for merge requests, it can be
   * whether it's merged or not, etc. Each source can decide what's "closed".
   *
   * @return bool
   *   Whether the issue is closed/finished or not.
   */
  public function isClosed(): bool;

  /**
   * Detect whether the given link is valid or not.
   *
   * @param bool $check_link_exists
   *   Check whether the link actually exists too.
   *
   * @return bool
   *   Whether the given link is valid or not.
   */
  public function isValid(bool $check_link_exists = TRUE): bool;

  /**
   * If the link is not valid, return the error.
   *
   * @return string
   *   Error string if the link is not valid.
   */
  public function getError(): string;

  /**
   * Return the list of users that participated in the issue.
   *
   * @param bool $extended
   *   Whether to retrieve full contribution information or just usernames.
   *
   * @return array[]
   *   List of contributors to the issue.
   */
  public function getContributors(bool $extended = TRUE): array;

  /**
   * Return an array with the activity (comments and reactions) made per user.
   *
   * @return array[]
   *   Array containing activity per username.
   */
  public function getActivity(): array;

  /**
   * Return an array with the related sources associated to the current one.
   *
   * @return string[]
   *   List of related links to the current source.
   */
  public function getRelatedLinks(): array;

}
