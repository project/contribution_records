<?php

namespace Drupal\contribution_records\Controller;

use Drupal\contribution_records\SourceLink;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Cache\CacheableJsonResponse;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\CacheableRedirectResponse;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\Url;
use Drupal\drupalorg\OrganizationService;
use Drupal\drupalorg\UserService;
use Drupal\node\NodeInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;
use Drupal\views\Views;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller to create contribution records or redirect to existing ones.
 */
class ContributionRecordController extends ControllerBase {

  /**
   * The source link service.
   *
   * @var \Drupal\contribution_records\SourceLink
   */
  protected SourceLink $sourceLink;

  /**
   * Queue factory service.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected QueueFactory $queueFactory;

  /**
   * User service from drupalorg module.
   *
   * @var \Drupal\drupalorg\UserService
   */
  protected UserService $userService;

  /**
   * Organization service from drupalorg module.
   *
   * @var \Drupal\drupalorg\OrganizationService
   */
  protected OrganizationService $organizationService;

  /**
   * The tempstore object.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected PrivateTempStoreFactory $tempStore;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('class_resolver')->getInstanceFromDefinition(SourceLink::class),
      $container->get('queue'),
      $container->get('drupalorg.user_service'),
      $container->get('drupalorg.organization_service'),
      $container->get('tempstore.private'),
    );
  }

  /**
   * Construct method.
   *
   * @param \Drupal\contribution_records\SourceLink $source_link
   *   Source link service.
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   Queue factory.
   * @param \Drupal\drupalorg\UserService $user_service
   *   User service from drupalorg module.
   * @param \Drupal\drupalorg\OrganizationService $organization_service
   *   Organization service from drupalorg module.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   The tempstore factory.
   */
  public function __construct(SourceLink $source_link, QueueFactory $queue_factory, UserService $user_service, OrganizationService $organization_service, PrivateTempStoreFactory $temp_store_factory) {
    $this->sourceLink = $source_link;
    $this->queueFactory = $queue_factory;
    $this->userService = $user_service;
    $this->organizationService = $organization_service;
    $this->tempStore = $temp_store_factory;
  }

  /**
   * Check if the source making the request is trusted.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result object.
   */
  public function executeImportAccess(?Request $request = NULL): AccessResultInterface {
    return ($this->checkSourceOfRequest($request)) ? AccessResult::allowed() : AccessResult::forbidden('Invalid token.');
  }

  /**
   * Checks the source of the request to see if it's allowed.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return bool
   *   Whether the request is allowed or not.
   */
  protected function checkSourceOfRequest(Request $request): bool {
    $drupalorg_token = $this->config('drupalorg.settings')->get('token');
    $request_token = $request->headers->get('Drupalorg-Webhook-Token');

    return (!empty($request_token) && ($request_token === $drupalorg_token));
  }

  /**
   * Import a contribution record forgoing validation as we trust the source.
   *
   * The source is validated in the access logic, meaning that the data is
   * fully curated and therefore trusted.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Response object.
   */
  public function import(Request $request): JsonResponse {
    $result = [
      'status' => 'error',
      'message' => $this->t('Not allowed'),
      'url' => NULL,
    ];

    // Process request and change result.
    $post = $request->request->all();
    if (
      empty($post['url']) ||
      empty($post['title']) ||
      empty($post['credits'])
    ) {
      $result['message'] = $this->t('Not all required fields are present.');
    }
    // Queue the request instead of processing it now.
    elseif ($request->request->get('queue', FALSE)) {
      $this->queueFactory->get('contribution_records_import_queue_worker')->createItem($post);
      $result['message'] = $this->t('Record was queued to import.');
      $result['status'] = 'success';
    }
    // Process it all otherwise.
    else {
      $source_link = $this->sourceLink->setLink($post['url']);
      if (!$source_link->isValid(FALSE)) {
        $result['message'] = $this->t('Link does not have the right format.');
      }
      // Do not process duplicates as these are in the new system already.
      elseif (($duplicate_node = $source_link->isDuplicated(TRUE))) {
        $result['message'] = $this->t('URL is already imported.');
        $result['url'] = $duplicate_node->toUrl()->setAbsolute()->toString();
        $result['status'] = 'warning';
      }
      else {
        $contrib_record = $source_link->createContribRecordFromRawData($post);
        if ($contrib_record) {
          $result['message'] = $this->t('Record was saved.');
          $result['url'] = $contrib_record->toUrl()->setAbsolute()->toString();
          $result['status'] = 'success';
        }
        else {
          $result['message'] = $this->t('Could not save the record.');
        }
      }
    }

    return new JsonResponse($result);
  }

  /**
   * Redirects to the contribution records by user jsonapi endpoint.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response.
   */
  public function byUsername(Request $request): RedirectResponse {
    $user_id = NULL;
    $username_param = $request->query->get('username');
    if (!empty($username_param)) {
      $username_param = urldecode($username_param);
      $user = $this->userService->getUserByUsername($username_param);
      $user_id = ($user instanceof UserInterface) ? $user->id() : -1;
    }

    $months = (int) $request->query->get('months', 0);
    $months = $months ? '-' . $months . ' months' : '';
    $project_machine_name = $request->query->get('machine_name');
    $page = (int) $request->query->get('page', 0);

    $query = [
      'views-argument' => [$user_id],
      'views-filter[changed]' => $months,
      'page' => $page,
    ];
    if (!is_null($project_machine_name)) {
      $query['views-filter[field_project_name_value]'] = $project_machine_name;
    }

    $destination = Url::fromRoute(
      'jsonapi_views.contribution_records.by_user',
      [],
      ['query' => $query]
    );
    return new RedirectResponse($destination->setAbsolute()->toString());
  }

  /**
   * Redirects to the contribution records by organization jsonapi endpoint.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response.
   */
  public function byOrganization(Request $request): RedirectResponse {
    $organization_id = NULL;
    $organization_param = $request->query->get('organization');
    if (!empty($organization_param)) {
      $organization_param = urldecode($organization_param);
      $organization = $this->organizationService->getOrganizationByTitle($organization_param);
      $organization_id = ($organization instanceof NodeInterface) ? $organization->id() : -1;
    }

    $months = (int) $request->query->get('months', 0);
    $months = $months ? '-' . $months . ' months' : NULL;
    $project_machine_name = $request->query->get('machine_name');
    $page = (int) $request->query->get('page', 0);

    $query = [
      'views-argument' => [$organization_id, $organization_id],
      'page' => $page,
    ];
    if (!is_null($project_machine_name)) {
      $query['views-filter[field_project_name_value]'] = $project_machine_name;
    }
    if (!is_null($months)) {
      $query['views-filter[changed]'] = $months;
    }

    $destination = Url::fromRoute(
      'jsonapi_views.contribution_records.by_organization',
      [],
      ['query' => $query]
    );
    return new RedirectResponse($destination->setAbsolute()->toString());
  }

  /**
   * Redirects to the contribution records by organization by user endpoint.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response.
   */
  public function byOrganizationByUser(Request $request): RedirectResponse {
    $organization_id = NULL;
    $organization_param = $request->query->get('organization');
    if (!empty($organization_param)) {
      $organization_param = urldecode($organization_param);
      $organization = $this->organizationService->getOrganizationByTitle($organization_param);
      $organization_id = ($organization instanceof NodeInterface) ? $organization->id() : -1;
    }

    $user_id = NULL;
    $username = $request->query->get('username');
    if (!empty($username)) {
      $user = $this->userService->getUserByUsername($username);
      $user_id = !is_null($user) ? $user->id() : -1;
    }

    $months = (int) $request->query->get('months', 0);
    $months = $months ? '-' . $months . ' months' : NULL;
    $project_machine_name = $request->query->get('machine_name');
    $page = (int) $request->query->get('page', 0);

    $query = [
      'views-argument' => [$organization_id, $organization_id],
      'page' => $page,
      // We will need the user UID which is in the paragraph, so include it.
      'include' => 'field_contributors',
    ];
    if (!is_null($user_id)) {
      $query['views-filter[field_contributor_user_target_id]'] = $user_id;
    }
    if (!is_null($months)) {
      $query['views-filter[changed]'] = $months;
    }
    if (!is_null($project_machine_name)) {
      $query['views-filter[field_project_name_value]'] = $project_machine_name;
    }

    $destination = Url::fromRoute(
      'jsonapi_views.contribution_records.by_organization_by_user',
      [],
      ['query' => $query]
    );
    return new RedirectResponse($destination->setAbsolute()->toString());
  }

  /**
   * Redirects to the contribution records by organization jsonapi endpoint.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   A redirect response.
   */
  public function metrics(Request $request): JsonResponse {
    $result = [
      'status' => 'error',
      'message' => $this->t('Not allowed'),
      'data' => [],
    ];
    // This is a very data-heavy endpoint, so limit the requests to places that
    // we trust. If we don't want to limit it, then remove below lines.
    if (!$this->checkSourceOfRequest($request)) {
      return new JsonResponse($result);
    }

    $display_param = $request->query->get('display');
    $display = in_array($display_param, [
      'credits',
      'demographics',
      'region',
      'account_age',
    ]) ?
      $display_param :
      NULL;
    if (is_null($display)) {
      $result['message'] = $this->t('Invalid display');
      return new JsonResponse($result);
    }

    $year = (int) $request->query->get('year');
    if (!$year || (strlen($year) !== 4)) {
      $result['message'] = $this->t('Invalid year (needs 4 digits)');
      return new JsonResponse($result);
    }

    $month = (int) $request->query->get('month');
    if ($month && ($month < 0 || $month > 12)) {
      $result['message'] = $this->t('Invalid month');
      return new JsonResponse($result);
    }

    $view = Views::getView('contribution_records_metrics');
    if (is_object($view)) {
      $view->setDisplay($display);

      $exposed_filters = $view->getExposedInput();
      if ($month) {
        $month = str_pad($month, 2, '0', STR_PAD_LEFT);
        $month_timestamp = strtotime($year . '-' . $month . '-01T00:00:00+00:00');
        $exposed_filters['field_last_status_change_value'] = [
          'min' => $month_timestamp,
          'max' => strtotime('+1 month', $month_timestamp),
        ];
      }
      else {
        // Whole year. Maybe too much data.
        $year_timestamp = strtotime($year . '-01-01T00:00:00+00:00');
        $exposed_filters['field_last_status_change_value'] = [
          'min' => $year_timestamp,
          'max' => strtotime('+1 year', $year_timestamp),
        ];
      }
      $view->setExposedInput($exposed_filters);
      $view->preExecute();
      $view->execute();
      $data = $view->result;

      $result['message'] = '';
      $result['status'] = $this->t('success');
      if (!empty($data)) {
        switch ($display) {
          case 'credits':
            $result['data'] = $this->formatMetricsCredits($data);
            break;

          case 'demographics':
            $result['data'] = $this->formatMetricsDemographics($data);
            break;

          case 'region':
            $result['data'] = $this->formatMetricsRegion($data);
            break;

          case 'account_age':
            $result['data'] = $this->formatMetricsAccountAge($data);
            break;
        }
      }
    }
    else {
      $result['message'] = $this->t('View not found');
    }

    return new JsonResponse($result);
  }

  /**
   * Adds user to contribution record.
   *
   * @param \Drupal\node\NodeInterface $node
   *   Node object.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response.
   */
  public function addMe(NodeInterface $node, Request $request): RedirectResponse {
    if ($node->getType() !== 'contribution_record') {
      return $this->redirect('<front>');
    }

    $source_link = $this->sourceLink->setLink($node->get('field_source_link')->getValue()[0]['uri']);
    $user = User::load($this->currentUser()->id());

    // Get last contribution defaults.
    $temp_store = $this->tempStore->get('contribution_records');
    $user_contribution_defaults = $temp_store->get('user_contribution_defaults');

    $defaults = [];
    if (!empty($user_contribution_defaults['volunteer']) && $user_contribution_defaults['volunteer']['value']) {
      $defaults['volunteer'] = 1;
    }
    if (!empty($user_contribution_defaults['attribute_orgs']) && $user_contribution_defaults['attribute_orgs']['value']) {
      $defaults['attribute_orgs'] = 1;
    }
    if (!empty($user_contribution_defaults['organization'])) {
      $defaults['organizations'] = [];
      foreach ($user_contribution_defaults['organization'] as $index => $org) {
        if (is_numeric($index) && !empty($org['target_id'])) {
          $defaults['organizations'][] = $org['target_id'];
        }
      }
    }
    if (!empty($user_contribution_defaults['customer'])) {
      $defaults['customers'] = [];
      foreach ($user_contribution_defaults['customer'] as $index => $org) {
        if (is_numeric($index) && !empty($org['target_id'])) {
          $defaults['customers'][] = $org['target_id'];
        }
      }
    }

    $source_link->addContributor($user, $node, $defaults);
    $this->messenger()->addMessage($this->t('You have been added as contributor.'));
    $destination = $node->toUrl('canonical', ['query' => ['open' => TRUE]]);
    return new RedirectResponse($destination->setAbsolute()->toString());
  }

  /**
   * Redirects existing contribution records or create them.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response.
   */
  public function process(Request $request): RedirectResponse {
    $source_link_param = $request->query->get('source_link');
    $destination = NULL;
    $contrib_record = NULL;

    if ($source_link_param) {
      $source_link = $this->sourceLink->setLink($source_link_param);

      if (!$source_link->isValid()) {
        $destination = Url::fromUserInput(
          '/node/add/contribution_record',
          [
            'query' => [
              'source_link' => $source_link->getLink(),
            ],
          ]
        );
      }
      elseif (($duplicate_node = $source_link->isDuplicated(TRUE))) {
        $contrib_record = $duplicate_node;
      }
      else {
        $contrib_record = $source_link->createContribRecord();
      }
    }
    else {
      $this->messenger()->addWarning($this->t('"source_link" parameter is required.'));
      $destination = Url::fromUserInput('/');
    }

    if ($contrib_record) {
      $format_param = $request->query->get('format');
      if ($format_param === 'jsonapi') {
        $destination = $this->getJsonapiUrl($contrib_record);
      }
      else {
        $destination = $contrib_record->toUrl();
      }
    }

    if ($destination instanceof Url) {
      $response = new CacheableRedirectResponse($destination->setAbsolute()->toString(TRUE)->getGeneratedUrl());
      if ($contrib_record) {
        $response->addCacheableDependency($contrib_record);
      }
      return $response;
    }

    // We should never get here, but fallback just in case.
    return $this->redirect('<front>');
  }

  /**
   * Returns source activity broken down by user.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return \Drupal\Core\Cache\CacheableJsonResponse
   *   Response object.
   */
  public function sourceActivityInformation(Request $request): CacheableJsonResponse {
    $result = [
      'status' => 'error',
      'data' => [],
    ];

    $source_link_param = $request->query->get('source_link');
    if ($source_link_param) {
      $source_link = $this->sourceLink->setLink($source_link_param);
      if (!$source_link->isValid()) {
        $result['message'] = $this->t('The "source_link" link is not valid or accepted.');
      }
      else {
        $result['status'] = 'success';
        $result['message'] = '';
        $result['data'] = [
          'meta' => [
            'title' => $source_link->getTitle(),
            'status' => $source_link->getStatus(),
            'link' => $source_link_param,
            'type' => $source_link->getSourceType(),
          ],
          'activity' => $source_link->getActivity(),
        ];
      }
    }
    else {
      $result['message'] = $this->t('"source_link" parameter is required.');
    }

    // Do not cache the response for maintainers.
    $maintainer = (bool) $request->query->get('maintainer');
    $max_age = $maintainer ? 0 : 600;
    $result['max_age'] = $max_age;

    // Cache the response.
    $response = new CacheableJsonResponse($result);
    $cache_metadata = (new CacheableMetadata())
      ->addCacheContexts([
        'url.query_args:source_link',
        'url.query_args:maintainer',
      ])
      ->setCacheMaxAge($max_age);
    $response->addCacheableDependency($cache_metadata);
    return $response;
  }

  /**
   * Returns the jsonapi URL of a contribution record node.
   *
   * @param \Drupal\node\NodeInterface $contrib_record
   *   Contribution record node.
   *
   * @return \Drupal\Core\Url
   *   Url object.
   */
  protected function getJsonapiUrl(NodeInterface $contrib_record): Url {
    return Url::fromRoute(
      'jsonapi.node--contribution_record.individual',
      ['entity' => $contrib_record->uuid()],
      [
        'query' => [
          // Related data in the same request.
          'include' => implode(',', [
            'field_contributors',
            'field_contributors.field_contributor_customer',
            'field_contributors.field_contributor_organisation',
            'field_contributors.field_contributor_user',
          ]),
        ],
      ]
    );
  }

  /**
   * Format the data for an aggregated display of credits.
   *
   * @param array $result
   *   Result from the view.
   *
   * @return array
   *   Formatted data.
   */
  protected function formatMetricsCredits(array $result): array {
    $data = [];
    /** @var \Drupal\views\ResultRow[] $result */
    foreach ($result as $row) {
      /** @var \Drupal\Core\Entity\EntityInterface $entity */
      $entity = $row->_entity;
      $contribution = $row->_relationship_entities['field_contributors'] ?? NULL;
      if ($entity->bundle() === 'contribution_record' && $contribution instanceof Paragraph) {
        $date = !empty($entity->get('field_last_status_change')->getValue()) ?
          $entity->get('field_last_status_change')->getValue()[0]['value'] :
          $entity->getChangedTime();
        $year_month = date('Y-m', $date);
        $volunteer = !empty($contribution->get('field_contributor_volunteer')->getValue()) ?
          (int) $contribution->get('field_contributor_volunteer')->getValue()[0]['value'] :
          0;
        $attribute_orgs = !empty($contribution->get('field_contributor_attribute_orgs')->getValue()) ?
          (int) $contribution->get('field_contributor_attribute_orgs')->getValue()[0]['value'] :
          0;
        $organization = 0;
        if ($attribute_orgs) {
          $organization = (int) (
            !empty($contribution->get('field_contributor_organisation')->getValue()) ||
            !empty($contribution->get('field_contributor_customer')->getValue())
          );
        }

        $volunteer_organization_key = (($volunteer) ? 'volunteer' : 'no_volunteer') . '|' . (($organization) ? 'organization' : 'no_organization');
        $data[$year_month][$volunteer_organization_key] = empty($data[$year_month][$volunteer_organization_key]) ? 1 : $data[$year_month][$volunteer_organization_key] + 1;
      }
    }

    return $data;
  }

  /**
   * Format the data for an aggregated display of credit demographics.
   *
   * @param array $result
   *   Result from the view.
   *
   * @return array
   *   Formatted data.
   */
  protected function formatMetricsDemographics(array $result): array {
    $data = [];
    /** @var \Drupal\views\ResultRow[] $result */
    foreach ($result as $row) {
      /** @var \Drupal\Core\Entity\EntityInterface $entity */
      $entity = $row->_entity;
      $contribution = $row->_relationship_entities['field_contributors'] ?? NULL;
      $user = $row->_relationship_entities['field_contributor_user'] ?? NULL;
      if (
        $entity->bundle() === 'contribution_record' &&
        $contribution instanceof Paragraph &&
        $user instanceof User
      ) {
        $date = !empty($entity->get('field_last_status_change')->getValue()) ?
          $entity->get('field_last_status_change')->getValue()[0]['value'] :
          $entity->getChangedTime();
        $year_month = date('Y-m', $date);

        $demographics_key = 'not_reported';
        if (!empty($user->get('field_demographics')->getValue())) {
          $demographics_value = $user->get('field_demographics')->getValue()[0]['value'];
          $demographics_key = match ($demographics_value) {
            'prefer not to answer' => 'prefer_not',
            'none' => 'none',
            default => 'underrepresented',
          };
        }

        $data[$year_month][$demographics_key] = empty($data[$year_month][$demographics_key]) ? 1 : $data[$year_month][$demographics_key] + 1;
      }
    }
    return $data;
  }

  /**
   * Format the data for an aggregated display of credit region.
   *
   * @param array $result
   *   Result from the view.
   *
   * @return array
   *   Formatted data.
   */
  protected function formatMetricsRegion(array $result): array {
    $data = [];
    /** @var \Drupal\views\ResultRow[] $result */
    foreach ($result as $row) {
      /** @var \Drupal\Core\Entity\EntityInterface $entity */
      $entity = $row->_entity;
      $contribution = $row->_relationship_entities['field_contributors'] ?? NULL;
      $user = $row->_relationship_entities['field_contributor_user'] ?? NULL;
      if (
        $entity->bundle() === 'contribution_record' &&
        $contribution instanceof Paragraph &&
        $user instanceof User
      ) {
        $date = !empty($entity->get('field_last_status_change')->getValue()) ?
          $entity->get('field_last_status_change')->getValue()[0]['value'] :
          $entity->getChangedTime();
        $year_month = date('Y-m', $date);

        $country_key = 'na';
        if (!empty($user->get('field_country')->getValue())) {
          $country_key = $user->get('field_country')->getValue()[0]['value'];
        }

        $data[$year_month][$country_key] = empty($data[$year_month][$country_key]) ? 1 : $data[$year_month][$country_key] + 1;
      }
    }
    return $data;
  }

  /**
   * Format the data for an aggregated display of account age.
   *
   * @param array $result
   *   Result from the view.
   *
   * @return array
   *   Formatted data.
   */
  protected function formatMetricsAccountAge(array $result): array {
    $data = [];
    /** @var \Drupal\views\ResultRow[] $result */
    foreach ($result as $row) {
      /** @var \Drupal\Core\Entity\EntityInterface $entity */
      $entity = $row->_entity;
      $contribution = $row->_relationship_entities['field_contributors'] ?? NULL;
      $user = $row->_relationship_entities['field_contributor_user'] ?? NULL;
      if (
        $entity->bundle() === 'contribution_record' &&
        $contribution instanceof Paragraph &&
        $user instanceof User
      ) {
        $date = !empty($entity->get('field_last_status_change')->getValue()) ?
          $entity->get('field_last_status_change')->getValue()[0]['value'] :
          $entity->getChangedTime();
        $year_month = date('Y-m', $date);

        // Account age in years.
        $created = (new \DateTime())->setTimestamp($user->getCreatedTime());
        $interval = (new \DateTime())->diff($created);
        $years = $interval->y;

        $data[$year_month][$years] = empty($data[$year_month][$years]) ? 1 : $data[$year_month][$years] + 1;
      }
    }
    return $data;
  }

}
