<?php

namespace Drupal\contribution_records\SourceLink;

use Drupal\Component\Serialization\Json;
use Drupal\contribution_records\SourceLinkInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * DrupalOrgIssue source implements issues coming from www.drupal.org.
 *
 * All logic should be about the source, no internal Drupal logic.
 */
class DrupalOrgIssue implements SourceLinkInterface {

  use StringTranslationTrait;

  /**
   * Domain of the source.
   *
   * @var string
   */
  public const DOMAIN = 'www.drupal.org';

  /**
   * Origin link to retrieve information from.
   *
   * @var string
   */
  public string $link;

  /**
   * The HTTP client to fetch the files with.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected ClientInterface $httpClient;

  /**
   * Error message if the link is not valid.
   *
   * @var string
   */
  protected string $errorMessage = '';

  /**
   * Whether the given link is valid or not.
   *
   * @var bool
   */
  protected bool $valid = FALSE;

  /**
   * Data obtained from querying the API of the source link.
   *
   * @var array|null
   *   Information coming from the source link.
   */
  protected $data = NULL;

  /**
   * Construct method.
   *
   * @param string $link
   *   Link to parse information from.
   */
  public function __construct(string $link) {
    $this->link = $link;
    // @phpstan-ignore-next-line
    $this->httpClient = \Drupal::httpClient();
  }

  /**
   * Fetches the data from the actual source via the API provided.
   */
  protected function fetchData() {
    // We're trying to fetch here, so change from NULL to empty array at least.
    $this->data = [];
    $response = NULL;

    // As the method is protected, it should only be called when the link is
    // valid or during the latest stages of the validation process, so
    // assume that the link is valid.
    $url_info = parse_url($this->link);
    $user_pass = '';
    if (!empty($url_info['user']) && !empty($url_info['pass'])) {
      $user_pass = $url_info['user'] . ':' . $url_info['pass'] . '@';
    }
    $api_link = $url_info['scheme'] . '://' . $user_pass . $url_info['host'] . '/api-d7' . $url_info['path'] . '.json';
    // Appending this will bring all the additional information that we need
    // for credits and comments.
    $api_link .= '?drupalorg_extra_credit=1&related_mrs=1';
    try {
      $response = $this->httpClient->request('GET', $api_link);
    }
    catch (ClientException $exception) {
      try {
        // Second time just in case.
        $response = $this->httpClient->request('GET', $api_link);
      }
      catch (ClientException $exception) {
        // Not much we can do at this point. Data will be set to empty array.
        $this->setError($this->t('Error fetching the link. Code: @code', ['@code' => $exception->getCode()]));
      }
    }

    if (($response instanceof ResponseInterface) && $response->getStatusCode() == Response::HTTP_OK) {
      $data = Json::decode($response->getBody()->getContents());

      // Validate the data received to make sure it's a project issue.
      if (empty($data['type']) || empty($data['status']) || empty($data['title']) || empty($data['field_issue_status'])) {
        $this->setError($this->t('Invalid node'));
      }
      elseif ($data['type'] !== 'project_issue') {
        $this->setError($this->t('Invalid type of node'));
      }
      elseif ($data['status'] !== '1') {
        $this->setError($this->t('Node is not published'));
      }
      else {
        // All checks have passed, link is valid so update the data property.
        $this->markAsValid();
        $this->data = $data;
      }
    }
  }

  /**
   * Helper method to set a link as valid.
   */
  protected function markAsValid() {
    $this->valid = TRUE;
    $this->errorMessage = '';
  }

  /**
   * Helper method to set a validation error.
   *
   * @param string $message
   *   Translated string.
   */
  protected function setError(string $message) {
    $this->valid = FALSE;
    $this->errorMessage = $message;
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle(): string {
    if (is_null($this->data)) {
      $this->fetchData();
    }

    return $this->data['title'] ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function getCreated(): int {
    if (is_null($this->data)) {
      $this->fetchData();
    }

    return $this->data['created'] ?? 0;
  }

  /**
   * {@inheritdoc}
   */
  public function getChanged(): int {
    if (is_null($this->data)) {
      $this->fetchData();
    }

    return $this->data['changed'] ?? 0;
  }

  /**
   * {@inheritdoc}
   */
  public function getLastStatusChanged(): int {
    if (is_null($this->data)) {
      $this->fetchData();
    }

    return $this->data['field_issue_last_status_change'] ?? $this->getChanged();
  }

  /**
   * {@inheritdoc}
   */
  public function getProject(): ?string {
    if (is_null($this->data)) {
      $this->fetchData();
    }

    return $this->data['field_project']['machine_name'] ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function getStatus(): string {
    if (is_null($this->data)) {
      $this->fetchData();
    }

    // List taken from `project_issue_open_states` function in D7.
    // @see https://git.drupalcode.org/project/project_issue/-/blob/7.x-2.x/project_issue.module#L2664-2671
    // 2 is 'Fixed', so it's removed from this list.
    $open_status_list = [1, 4, 8, 13, 14, 15, 16];
    $status = $this->data['field_issue_status'] ?? 0;
    return in_array($status, $open_status_list) ? SourceLinkInterface::STATUS_OPEN : SourceLinkInterface::STATUS_CLOSED;
  }

  /**
   * {@inheritdoc}
   */
  public function isClosed(): bool {
    return ($this->getStatus() === SourceLinkInterface::STATUS_CLOSED);
  }

  /**
   * {@inheritdoc}
   */
  public function isValid(bool $check_link_exists = TRUE): bool {
    // Default error.
    $this->setError($this->t('Link is not valid'));

    $url_info = parse_url($this->link);
    if (!empty($url_info)) {
      if ($url_info['scheme'] !== 'https') {
        $this->setError($this->t('Protocol should be https'));
      }
      elseif ($url_info['host'] !== static::DOMAIN) {
        $this->setError($this->t('Domain should be @domain', ['@domain' => static::DOMAIN]));
      }
      elseif (!empty($url_info['query'])) {
        $this->setError($this->t('Link should not have query parameters in it'));
      }
      elseif (!empty($url_info['fragment'])) {
        $this->setError($this->t('Link should not have anchors in it'));
      }
      elseif (empty($url_info['path'])) {
        $this->setError($this->t('Link cannot be just the domain'));
      }
      elseif (!preg_match('/^\/node\/[0-9]+$/', $url_info['path'])) {
        $this->setError($this->t('Link should be canonical. ie: @link', ['@link' => 'https://' . static::DOMAIN . '/node/123']));
      }
      elseif ($check_link_exists) {
        // No further checks on the URL, time to fetch the actual URL and see
        // if the information returned is valid. The method will update the
        // 'valid' property and error message as needed.
        $this->fetchData();
      }
      else {
        // All checks passed, but we didn't need to check if the link exists.
        // Format-wise, we can say it's valid.
        $this->markAsValid();
      }
    }

    return $this->valid;
  }

  /**
   * {@inheritdoc}
   */
  public function getError(): string {
    return $this->valid ? '' : $this->errorMessage;
  }

  /**
   * {@inheritdoc}
   */
  public function getActivity(): array {
    if (is_null($this->data)) {
      $this->fetchData();
    }

    $comments = $this->data['comments'] ?? [];
    $files = $this->data['field_issue_files'] ?? [];
    $files_by_cid = [];
    if (!empty($files)) {
      // Counts by comment id.
      foreach ($files as $file) {
        if (empty($files_by_cid[$file['file']['cid']])) {
          $files_by_cid[$file['file']['cid']] = 0;
        }
        $files_by_cid[$file['file']['cid']]++;
      }
    }

    $activity = [];
    foreach ($comments as $comment) {
      $username = $comment['data']['username'];
      if (empty($activity[$username])) {
        $activity[$username] = [
          'comments' => 0,
          'files' => 0,
        ];
      }
      $activity[$username]['comments']++;
      if (!empty($files_by_cid[$comment['id']])) {
        $activity[$username]['files'] += $files_by_cid[$comment['id']];
      }
    }

    return $activity;
  }

  /**
   * {@inheritdoc}
   */
  public function getContributors(bool $extended = TRUE): array {
    if (is_null($this->data)) {
      $this->fetchData();
    }

    $contributors = [];
    $credits = $this->data['field_issue_credit'] ?? [];
    $comments = $this->data['comments'] ?? [];

    $credited_comments = array_column($credits, 'id');
    foreach ($comments as $comment) {
      if (!empty($comment['data'])) {
        $attribute_to = in_array(0, $comment['data']['field_attribute_as_volunteer']);
        $organizations = NULL;
        $customers = NULL;
        if ($attribute_to) {
          if (!empty($comment['data']['field_attribute_contribution_to'])) {
            $organizations = [];
            foreach ($comment['data']['field_attribute_contribution_to'] as $item) {
              $organizations[] = $item['title'];
            }
            $organizations = implode(',', $organizations);
          }

          if (!empty($comment['data']['field_for_customer'])) {
            $customers = [];
            foreach ($comment['data']['field_for_customer'] as $item) {
              $customers[] = $item['title'];
            }
            $customers = implode(',', $customers);
          }
        }

        $contributors[$comment['data']['username']] = [
          'username' => $comment['data']['username'],
          'volunteer' => in_array(1, $comment['data']['field_attribute_as_volunteer']),
          'organizations' => $organizations,
          'customers' => $customers,
          'credit' => in_array($comment['id'], $credited_comments),
        ];
      }
    }

    return $extended ? $contributors : array_keys($contributors);
  }

  /**
   * {@inheritdoc}
   */
  public function getRelatedLinks(): array {
    if (is_null($this->data)) {
      $this->fetchData();
    }

    return $this->data['related_mrs'] ?? [];
  }

}
