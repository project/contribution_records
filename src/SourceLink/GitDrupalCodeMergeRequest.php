<?php

namespace Drupal\contribution_records\SourceLink;

/**
 * GitDrupalCodeMergeRequest source implements git.drupalcode.org merge request.
 *
 * All logic should be about the source, no internal Drupal logic.
 */
class GitDrupalCodeMergeRequest extends GitDrupalCodeBase {

  /**
   * {@inheritdoc}
   */
  public const API_METHOD = 'mergeRequests';

  /**
   * {@inheritdoc}
   */
  public const API_PATH_COMPONENT = 'merge_requests';

}
