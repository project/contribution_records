<?php

namespace Drupal\contribution_records\SourceLink;

use Drupal\contribution_records\SourceLinkInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\drupalorg\Traits\GitLabClientTrait;
use Gitlab\ResultPager;
use MarufMax\Emoticon\Emoticon;

/**
 * Base methods for sources from git.drupalcode.org.
 *
 * All logic should be about the source, no internal Drupal logic.
 */
abstract class GitDrupalCodeBase implements SourceLinkInterface {

  use StringTranslationTrait;
  use GitLabClientTrait;

  /**
   * Domain of the source.
   *
   * @var string
   */
  public const DOMAIN = 'git.drupalcode.org';

  /**
   * The method to call when retrieving information.
   *
   * This is expected to be overridden by child classes.
   *
   * @var string
   */
  public const API_METHOD = '';

  /**
   * The method to call when retrieving information.
   *
   * This is expected to be overridden by child classes.
   *
   * @var string
   */
  public const API_PATH_COMPONENT = '';

  /**
   * Origin link to retrieve information from.
   *
   * @var string
   */
  public string $link;

  /**
   * Data obtained from querying the API of the source link.
   *
   * @var array|null
   *   Information coming from the source link.
   */
  protected $data = NULL;

  /**
   * Error message if the link is not valid.
   *
   * @var string
   */
  protected string $errorMessage = '';

  /**
   * Whether the given link is valid or not.
   *
   * @var bool
   */
  protected bool $valid = FALSE;

  /**
   * Construct method.
   *
   * @param string $link
   *   Link to parse information from.
   */
  public function __construct(string $link) {
    $this->link = $link;
  }

  /**
   * Helper method to set a link as valid.
   */
  protected function markAsValid() {
    $this->valid = TRUE;
    $this->errorMessage = '';
  }

  /**
   * Helper method to set a validation error.
   *
   * @param string $message
   *   Translated string.
   */
  protected function setError(string $message) {
    $this->valid = FALSE;
    $this->errorMessage = $message;
  }

  /**
   * Fetches the data from the actual source via the API provided.
   */
  protected function fetchData() {
    if (empty(static::API_METHOD)) {
      $this->setError($this->t('Source plugin not configured correctly.'));
      return [];
    }

    // We're trying to fetch here, so change from NULL to empty array at least.
    $this->data = [];

    $gitlab_client = $this->getGitLabClient();

    // As the method is protected, it should only be called when the link is
    // valid or during the latest stages of the validation process, so
    // assume that the link is valid.
    $url_info = parse_url($this->link);
    $path_parts = explode('/', trim($url_info['path'], '/'));
    // ie: https://git.drupalcode.org/project/drupalorg/-/issues/1
    // ie: https://git.drupalcode.org/project/drupalorg/-/merge_requests/2
    $namespace = $path_parts[0] ?? FALSE;
    $project = $path_parts[1] ?? FALSE;
    $iid = $path_parts[4] ?? FALSE;
    if ($namespace && $project && $iid) {
      $this->data['project'] = $project;
      try {
        $data = $gitlab_client->{static::API_METHOD}()->show($namespace . '/' . $project, $iid);
        if (!empty($data)) {
          // All checks have passed, link is valid, and we have data, so update
          // the data property.
          $this->markAsValid();
          $this->data += $data;
        }
      }
      catch (\Throwable $e) {
        $this->setError($this->t('GitLab API error. Code: @code', ['@code' => $e->getCode()]));
      }
    }
    else {
      $this->setError($this->t('Could not extract project or iid from the URL.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle(): string {
    if (is_null($this->data)) {
      $this->fetchData();
    }

    return $this->data['title'] ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function getProject(): ?string {
    if (is_null($this->data)) {
      $this->fetchData();
    }

    return $this->data['project'] ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function getStatus(): string {
    if (is_null($this->data)) {
      $this->fetchData();
    }

    return (!empty($this->data['state']) && ($this->data['state'] === 'opened')) ? SourceLinkInterface::STATUS_OPEN : SourceLinkInterface::STATUS_CLOSED;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreated(): int {
    if (is_null($this->data)) {
      $this->fetchData();
    }

    return !empty($this->data['created_at']) ? strtotime($this->data['created_at']) : 0;
  }

  /**
   * {@inheritdoc}
   */
  public function getChanged(): int {
    if (is_null($this->data)) {
      $this->fetchData();
    }

    return !empty($this->data['updated_at']) ? strtotime($this->data['updated_at']) : 0;
  }

  /**
   * {@inheritdoc}
   */
  public function getLastStatusChanged(): int {
    if (is_null($this->data)) {
      $this->fetchData();
    }

    if ($this->isClosed()) {
      return !empty($this->data['closed_at']) ? strtotime($this->data['closed_at']) : $this->getChanged();
    }

    // If the issue is not closed get the changed timestamp.
    return $this->getChanged();
  }

  /**
   * {@inheritdoc}
   */
  public function isClosed(): bool {
    return ($this->getStatus() == SourceLinkInterface::STATUS_CLOSED);
  }

  /**
   * {@inheritdoc}
   *
   * Examples:
   * - https://git.drupalcode.org/project/drupal/-/issues/123
   * - https://gitlab1.code-dev.devdrupal.org/project/examples/-/merge_requests/456
   */
  public function isValid(bool $check_link_exists = TRUE): bool {
    if (empty(static::API_PATH_COMPONENT)) {
      $this->setError($this->t('Source plugin not configured correctly.'));
      return FALSE;
    }

    // Default error.
    $this->setError($this->t('Link is not valid'));

    $url_info = parse_url($this->link);
    if (!empty($url_info)) {
      if ($url_info['scheme'] !== 'https') {
        $this->setError($this->t('Protocol should be https'));
      }
      elseif ($url_info['host'] !== static::DOMAIN) {
        $this->setError($this->t('Domain should be @domain', ['@domain' => static::DOMAIN]));
      }
      elseif (!empty($url_info['query'])) {
        $this->setError($this->t('Link should not have query parameters in it'));
      }
      elseif (!empty($url_info['fragment'])) {
        $this->setError($this->t('Link should not have anchors in it'));
      }
      elseif (empty($url_info['path'])) {
        $this->setError($this->t('Link cannot be just the domain'));
      }
      elseif (!preg_match('/^\/(project|issue)\/[a-zA-Z0-9_\-]+\/-\/' . static::API_PATH_COMPONENT . '\/[0-9]+$/', $url_info['path'])) {
        $this->setError($this->t('Link should be canonical. ie: @link', ['@link' => 'https://' . static::DOMAIN . '/project/project_name/-/' . static::API_PATH_COMPONENT . '/123']));
      }
      elseif ($check_link_exists) {
        // No further checks on the URL, time to fetch the actual URL and see
        // if the information returned is valid. The method will update the
        // 'valid' property and error message as needed.
        $this->fetchData();
      }
      else {
        // All checks passed, but we didn't need to check if the link exists.
        // Format-wise, we can say it's valid.
        $this->markAsValid();
      }
    }

    return $this->valid;
  }

  /**
   * {@inheritdoc}
   */
  public function getError(): string {
    return $this->valid ? '' : $this->errorMessage;
  }

  /**
   * Gets the number of comments of an issue by user.
   *
   * @return array
   *   List of users that made a comment and the number of comments.
   */
  protected function getComments(): array {
    if (is_null($this->data)) {
      $this->fetchData();
    }

    if (!empty($this->data['comments'])) {
      return $this->data['comments'];
    }

    $gitlab_client = $this->getGitLabClient();
    $project_id = $this->data['project_id'] ?? FALSE;
    $iid = $this->data['iid'] ?? FALSE;
    $comments_per_user = [];
    if ($project_id && $iid) {
      try {
        $pager = new ResultPager($gitlab_client);
        $comments = $pager->fetchAll(
          $gitlab_client->{static::API_METHOD}(),
          'showNotes',
          [$project_id, $iid]
        );
        if (!empty($comments)) {
          foreach ($comments as $comment) {
            if (empty($comments_per_user[$comment['author']['username']])) {
              $comments_per_user[$comment['author']['username']] = 0;
            }
            $comments_per_user[$comment['author']['username']]++;
          }
          $this->data['comments'] = $comments_per_user;
        }
      }
      catch (\Throwable $e) {
        $this->setError($this->t('GitLab API error. Code: @code', ['@code' => $e->getCode()]));
      }
    }
    else {
      $this->setError($this->t('Could not get the information from GitLab.'));
    }

    return $comments_per_user;
  }

  /**
   * Gets the reactions of an issue by user.
   *
   * @return array
   *   List of users and their reactions within the issue.
   */
  protected function getReactions(): array {
    if (is_null($this->data)) {
      $this->fetchData();
    }

    if (!empty($this->data['reactions'])) {
      return $this->data['reactions'];
    }

    $gitlab_client = $this->getGitLabClient();
    $project_id = $this->data['project_id'] ?? FALSE;
    $iid = $this->data['iid'] ?? FALSE;
    $reactions_per_user = [];
    if ($project_id && $iid) {
      try {
        $pager = new ResultPager($gitlab_client);
        $reactions = $pager->fetchAll(
          $gitlab_client->{static::API_METHOD}(),
          'awardEmoji',
          [$project_id, $iid]
        );
        if (!empty($reactions)) {
          foreach ($reactions as $reaction) {
            $reactions_per_user[$reaction['user']['username']][] = (new Emoticon())->get($reaction['name']) ?: $reaction['name'];
          }
          $this->data['reactions'] = $reactions_per_user;
        }
      }
      catch (\Throwable $e) {
        $this->setError($this->t('GitLab API error. Code: @code', ['@code' => $e->getCode()]));
      }
    }
    else {
      $this->setError($this->t('Could not get the information from GitLab.'));
    }

    return $reactions_per_user;
  }

  /**
   * {@inheritdoc}
   */
  public function getActivity(): array {
    if (is_null($this->data)) {
      $this->fetchData();
    }

    // Activity is made up of comments and reactions. We want the number of
    // comments and the list reactions per user.
    $comments = $this->getComments();
    $reactions = $this->getReactions();
    $all_users = array_unique(array_merge(array_keys($comments), array_keys($reactions)));

    $activity = [];
    foreach ($all_users as $username) {
      $activity[$username] = [
        'comments' => $comments[$username] ?? 0,
        'reactions' => $reactions[$username] ?? [],
      ];
    }

    return $activity;
  }

  /**
   * {@inheritdoc}
   */
  public function getContributors(bool $extended = TRUE): array {
    if (is_null($this->data)) {
      $this->fetchData();
    }

    if (!empty($this->data['contributors'])) {
      return $extended ? $this->data['contributors'] : array_keys($this->data['contributors']);
    }

    $gitlab_client = $this->getGitLabClient();
    $project_id = $this->data['project_id'] ?? FALSE;
    $iid = $this->data['iid'] ?? FALSE;
    $contributors = [];
    if ($project_id && $iid) {
      try {
        $pager = new ResultPager($gitlab_client);
        // 'showParticipants' will get comments and reactions.
        $participants = $pager->fetchAll(
          $gitlab_client->{static::API_METHOD}(),
          'showParticipants',
          [$project_id, $iid]
        );
        if (!empty($participants)) {
          foreach ($participants as $participant) {
            $username = $participant['username'] ?? $participant['author']['username'] ?? NULL;
            if ($username) {
              $contributors[$username] = [
                'username' => $username,
                // There is nothing in GitLab to infer the following values.
                'volunteer' => FALSE,
                'organizations' => NULL,
                'customers' => NULL,
                'credit' => FALSE,
              ];
            }
          }

          $this->data['contributors'] = $contributors;
        }
      }
      catch (\Throwable $e) {
        $this->setError($this->t('GitLab API error. Code: @code', ['@code' => $e->getCode()]));
      }
    }
    else {
      $this->setError($this->t('Could not get the information from GitLab.'));
    }

    return $extended ? $contributors : array_keys($contributors);
  }

  /**
   * {@inheritdoc}
   */
  public function getRelatedLinks(): array {
    return [];
  }

}
