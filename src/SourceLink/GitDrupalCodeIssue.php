<?php

namespace Drupal\contribution_records\SourceLink;

/**
 * GitDrupalCodeIssue source implements issues coming from git.drupalcode.org.
 *
 * All logic should be about the source, no internal Drupal logic.
 */
class GitDrupalCodeIssue extends GitDrupalCodeBase {

  /**
   * {@inheritdoc}
   */
  public const API_METHOD = 'issues';

  /**
   * {@inheritdoc}
   */
  public const API_PATH_COMPONENT = 'issues';

  /**
   * {@inheritdoc}
   */
  public function getRelatedLinks(): array {
    if (is_null($this->data)) {
      $this->fetchData();
    }

    $client = $this->getGitLabClient();
    $project_id = $this->data['project_id'] ?? FALSE;
    $internal_issue_id = $this->data['iid'] ?? FALSE;
    $related_links = [];

    if ($project_id && $internal_issue_id) {
      try {
        $merge_requests = $client->issues()->relatedMergeRequests($project_id, $internal_issue_id);
        foreach ($merge_requests as $mr) {
          $related_links[] = $mr['web_url'];
        }
      }
      catch (\Throwable $e) {
        // Couldn't find any.
      }
    }

    return $related_links;
  }

}
