<?php

namespace Drupal\contribution_records\SourceLink;

/**
 * This source implements MRs coming from gitlab1.code-dev.devdrupal.org.
 *
 * All logic should be about the source, no internal Drupal logic.
 */
class DevGitDrupalCodeMergeRequest extends GitDrupalCodeMergeRequest {

  /**
   * Domain of the source.
   *
   * @var string
   */
  public const DOMAIN = 'gitlab1.code-dev.devdrupal.org';

}
