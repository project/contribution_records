<?php

namespace Drupal\contribution_records\SourceLink;

/**
 * This source implements issues coming from dev copies of drupal.org.
 *
 * All logic should be about the source, no internal Drupal logic.
 */
class DevDrupalOrgIssue extends DrupalOrgIssue {

  /**
   * Domain of the source.
   *
   * @var string
   */
  public const DOMAIN = 'fjgarlin-drupal.dev.devdrupal.org';
  // @todo Replace above with below when this is merged https://www.drupal.org/project/drupalorg/issues/3325757
  // public const DOMAIN = 'gitlab1-drupal.dev.devdrupal.org';
}
