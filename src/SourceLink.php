<?php

namespace Drupal\contribution_records;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\contribution_records\SourceLink\DevDrupalOrgIssue;
use Drupal\contribution_records\SourceLink\DevGitDrupalCodeIssue;
use Drupal\contribution_records\SourceLink\DevGitDrupalCodeMergeRequest;
use Drupal\contribution_records\SourceLink\DrupalOrgIssue;
use Drupal\contribution_records\SourceLink\GitDrupalCodeBase;
use Drupal\contribution_records\SourceLink\GitDrupalCodeIssue;
use Drupal\contribution_records\SourceLink\GitDrupalCodeMergeRequest;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\drupalorg\ProjectService;
use Drupal\drupalorg\UserService;
use Drupal\node\NodeInterface;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Generic Source class to wrap the multiple kinds of sources that we can have.
 *
 * This class will usually communicate with the specific source classes
 * (ie: DrupalOrgIssue) and also to the Drupal database directly.
 */
class SourceLink implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * Import revision message.
   *
   * @var string
   */
  public const IMPORT_MESSAGE = 'AUTOMATED-IMPORT';

  /**
   * Specific source based on the link given.
   *
   * @var \Drupal\contribution_records\SourceLinkInterface|null
   */
  protected ?SourceLinkInterface $source = NULL;

  /**
   * Origin link to retrieve information from.
   *
   * @var string|null
   */
  public ?string $link = NULL;

  /**
   * Wrapper object for the configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $config;

  /**
   * The cache service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected CacheBackendInterface $cache;

  /**
   * The database connection used to store flood event information.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $connection;

  /**
   * An instance of the entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Drupalorg project service.
   *
   * @var \Drupal\drupalorg\ProjectService
   */
  protected ProjectService $projectService;

  /**
   * Drupalorg user service.
   *
   * @var \Drupal\drupalorg\UserService
   */
  protected UserService $userService;

  /**
   * Logger factory service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected LoggerChannelFactory $loggerFactory;

  /**
   * Time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected TimeInterface $timeService;

  /**
   * Construct method.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache service.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager service.
   * @param \Drupal\drupalorg\ProjectService $projectService
   *   Project service for drupalorg projects.
   * @param \Drupal\drupalorg\UserService $userService
   *   User service for drupalorg users.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $loggerFactory
   *   Logger service.
   * @param \Drupal\Component\Datetime\TimeInterface $timeService
   *   Time service.
   */
  public function __construct(
    ConfigFactoryInterface $configFactory,
    CacheBackendInterface $cache,
    Connection $connection,
    EntityTypeManagerInterface $entityTypeManager,
    ProjectService $projectService,
    UserService $userService,
    LoggerChannelFactory $loggerFactory,
    TimeInterface $timeService,
  ) {
    $this->config = $configFactory;
    $this->cache = $cache;
    $this->connection = $connection;
    $this->entityTypeManager = $entityTypeManager;
    $this->projectService = $projectService;
    $this->userService = $userService;
    $this->loggerFactory = $loggerFactory;
    $this->timeService = $timeService;
  }

  /**
   * Create instance of the class.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Container service.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('cache.data'),
      $container->get('database'),
      $container->get('entity_type.manager'),
      $container->get('drupalorg.project_service'),
      $container->get('drupalorg.user_service'),
      $container->get('logger.factory'),
      $container->get('datetime.time')
    );
  }

  /**
   * Sets the link for the source.
   *
   * @param string $link
   *   Link to set.
   *
   * @return SourceLink
   *   SourceLink object.
   */
  public function setLink(string $link): SourceLink {
    $this->link = $link;
    $this->detectSource();
    return $this;
  }

  /**
   * Calls the different source classes based on the URL of the link.
   *
   * Possible sources are:
   * - www.drupal.org issue
   * - git.drupalcode.org issue
   * - git.drupalcode.org merge request
   * - If "allow_dev_sources" is enabled, then dev-URLs of the above domains
   *   will be allowed.
   */
  protected function detectSource() {
    $this->source = NULL;

    // Issues from www.drupal.org.
    if (str_contains($this->link, DrupalOrgIssue::DOMAIN)) {
      $this->source = new DrupalOrgIssue($this->link);
    }
    // Issues or MRs from the same GitLab instance.
    elseif (
      GitDrupalCodeIssue::DOMAIN === GitDrupalCodeMergeRequest::DOMAIN &&
      str_contains($this->link, GitDrupalCodeIssue::DOMAIN)
    ) {
      if (str_contains($this->link, '/' . GitDrupalCodeIssue::API_PATH_COMPONENT . '/')) {
        $this->source = new GitDrupalCodeIssue($this->link);
      }
      elseif (str_contains($this->link, '/' . GitDrupalCodeMergeRequest::API_PATH_COMPONENT . '/')) {
        $this->source = new GitDrupalCodeMergeRequest($this->link);
      }
    }
    elseif ($this->config->get('contribution_records.settings')->get('allow_dev_sources')) {
      if (str_contains($this->link, DevDrupalOrgIssue::DOMAIN)) {
        $this->source = new DevDrupalOrgIssue($this->link);
      }
      elseif (
        DevGitDrupalCodeIssue::DOMAIN === DevGitDrupalCodeMergeRequest::DOMAIN &&
        str_contains($this->link, DevGitDrupalCodeIssue::DOMAIN)
      ) {
        if (str_contains($this->link, '/' . DevGitDrupalCodeIssue::API_PATH_COMPONENT . '/')) {
          $this->source = new DevGitDrupalCodeIssue($this->link);
        }
        elseif (str_contains($this->link, '/' . DevGitDrupalCodeMergeRequest::API_PATH_COMPONENT . '/')) {
          $this->source = new DevGitDrupalCodeMergeRequest($this->link);
        }
      }
    }
  }

  /**
   * Returns the title of the issue.
   *
   * @return string
   *   Title given to the issue.
   */
  public function getTitle(): string {
    return $this->source?->getTitle() ?? '';
  }

  /**
   * Returns the original link that was given.
   *
   * @return string
   *   Link value.
   */
  public function getLink(): string {
    return $this->link;
  }

  /**
   * Determines whether the link is valid or not.
   *
   * @param bool $check_link_exists
   *   Also check whether the link actually exists.
   *
   * @return bool
   *   Valid link or not.
   */
  public function isValid(bool $check_link_exists = TRUE): bool {
    return $this->source && $this->source->isValid($check_link_exists);
  }

  /**
   * Returns the last error, if any.
   *
   * @return string
   *   String representation of the error.
   */
  public function getError(): string {
    return $this->source?->getError() ?? $this->t('Domain is not valid');
  }

  /**
   * Returns the project associated to the link.
   *
   * @return \Drupal\node\NodeInterface|null
   *   Project associated to the link.
   */
  public function getProject(): ?NodeInterface {
    $cache_id = 'contribution_records:project:' . $this->getLink();
    $cache = $this->cache->get($cache_id);
    if ($cache) {
      return $cache->data;
    }
    else {
      $project = NULL;
      $project_string = $this->source?->getProject() ?? FALSE;
      if ($project_string && $this->source instanceof DrupalOrgIssue) {
        $project = $this->projectService->getProjectByMachineName($project_string);
      }
      elseif ($project_string && $this->source instanceof GitDrupalCodeBase) {
        $composer_namespace = 'drupal/' . $project_string;
        $project = $this->projectService->getProjectByComposerNamespace($composer_namespace);
      }

      $this->cache->set($cache_id, $project);
      return $project;
    }
  }

  /**
   * Return the list of users that participated in the issue.
   *
   * @param bool $extended
   *   Whether to retrieve full contribution information or just users.
   *
   * @return \Drupal\user\UserInterface[]|array[]
   *   List of contributors to the issue.
   */
  public function getContributors(bool $extended = TRUE): array {
    $cache_id = 'contribution_records:contributors:' . (int) $extended . ':' . $this->getLink();
    $cache = $this->cache->get($cache_id);
    if ($cache) {
      return $cache->data;
    }
    else {
      $contributors_info = $this->source?->getContributors($extended) ?? [];

      $contributors = [];
      if ($extended === FALSE) {
        // Only users needed.
        if ($this->source instanceof GitDrupalCodeBase) {
          $contributors = $this->userService->getUsersByGitUsername($contributors_info) ?? [];
        }
        elseif ($this->source instanceof DrupalOrgIssue) {
          $contributors = $this->userService->getUsersByUsername($contributors_info) ?? [];
        }
      }
      else {
        $method = '';
        if ($this->source instanceof GitDrupalCodeBase) {
          $method = 'getUserByGitUsername';
        }
        elseif ($this->source instanceof DrupalOrgIssue) {
          $method = 'getUserByUsername';
        }

        foreach ($contributors_info as $extended_info) {
          $user = $this->userService->{$method}($extended_info['username']);
          $organizations = $extended_info['organizations'] ? $this->getOrganizations(explode(',', $extended_info['organizations'])) : NULL;
          $customers = $extended_info['customers'] ? $this->getOrganizations(explode(',', $extended_info['customers'])) : NULL;
          $contributors[] = [
            'user' => $user,
            'volunteer' => (bool) $extended_info['volunteer'],
            'organizations' => $organizations,
            'customers' => $customers,
            'credit' => (bool) $extended_info['credit'],
          ];
        }
      }

      // There are background tasks syncing contributors so keep the time low.
      $this->cache->set($cache_id, $contributors, strtotime('+1 minute'));
      return $contributors;
    }
  }

  /**
   * Return an array with the activity (comments and reactions) made per user.
   *
   * @return array[]
   *   Array containing activity per username.
   */
  public function getActivity(): array {
    $method = '';
    if ($this->source instanceof GitDrupalCodeBase) {
      $method = 'getUserByGitUsername';
    }
    elseif ($this->source instanceof DrupalOrgIssue) {
      $method = 'getUserByUsername';
    }
    else {
      return [];
    }

    $activity = [];
    $source_activity = $this->source?->getActivity() ?? [];
    if (!empty($source_activity)) {
      foreach ($source_activity as $username => $user_activity) {
        /** @var \Drupal\user\UserInterface $user */
        $user = $this->userService->{$method}($username);
        if ($user) {
          $activity[$user->getAccountName()] = [
            'comments' => $user_activity['comments'] ?? 0,
            'files' => $user_activity['files'] ?? 0,
            'reactions' => $user_activity['reactions'] ?? [],
          ];
        }
      }
    }

    return $activity;
  }

  /**
   * Return plain links to related sources.
   *
   * @return string[]
   *   Links to related sources.
   */
  public function getRelatedLinks(): array {
    $cache_id = 'contribution_records:related_links:' . $this->getLink();
    $cache = $this->cache->get($cache_id);
    if ($cache) {
      return $cache->data;
    }
    else {
      $related_links = $this->source?->getRelatedLinks() ?? [];
      $this->cache->set($cache_id, $related_links, strtotime('+5 minutes'));
      return $related_links;
    }
  }

  /**
   * Returns whether the given user is a maintainer of the project or not.
   *
   * @param \Drupal\user\Entity\User $user
   *   User to check.
   *
   * @return bool
   *   Whether the user is a maintainer of the project connected to the link.
   */
  public function isMaintainer(User $user): bool {
    foreach ($this->getProjectMaintainers() as $maintainer) {
      if ($maintainer->id() == $user->id()) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Return the list of maintainers for the project.
   *
   * @return Drupal\user\Entity\User[]
   *   List of maintainers of the project.
   */
  public function getProjectMaintainers(): array {
    $cache_id = 'contribution_records:maintainers:' . $this->getLink();
    $cache = $this->cache->get($cache_id);
    if ($cache) {
      return $cache->data;
    }
    else {
      $maintainers = [];
      $project = $this->getProject();
      if ($project) {
        $maintainers = $this->projectService->getProjectMaintainers($project);
      }

      $this->cache->set($cache_id, $maintainers, strtotime('+1 hour'));
      return $maintainers;
    }
  }

  /**
   * Returns the current status of a given link.
   *
   * @return string
   *   Status string for the issue.
   */
  public function getStatus(): string {
    return $this->source?->getStatus() ?? '';
  }

  /**
   * Returns the created timestamp.
   *
   * @return int
   *   Created timestamp.
   */
  public function getCreated(): int {
    return $this->source?->getCreated() ?? 0;
  }

  /**
   * Returns the changed timestamp.
   *
   * @return int
   *   Changed timestamp.
   */
  public function getChanged(): int {
    return $this->source?->getChanged() ?? 0;
  }

  /**
   * Returns the time the status was last changed.
   *
   * @return int
   *   Timestamp when the status was last changed.
   */
  public function getLastStatusChanged(): int {
    return $this->source?->getLastStatusChanged() ?? 0;
  }

  /**
   * Returns whether the connected contribution is considered closed.
   *
   * @return bool
   *   Whether the contribution is closed or not.
   */
  public function isClosed(): bool {
    return $this->source?->isClosed() ?? FALSE;
  }

  /**
   * Detects if a given link is already on the system.
   *
   * @param bool $return_node
   *   Return the duplicate node instead of just the flag.
   *
   * @return bool|\Drupal\node\NodeInterface
   *   Whether the link already exists or not or the node if requested.
   */
  public function isDuplicated(bool $return_node = FALSE): bool | NodeInterface {
    $id = $this->entityTypeManager->getStorage('node')->getQuery()
      ->accessCheck(FALSE)
      ->condition('status', NodeInterface::PUBLISHED)
      ->condition('type', 'contribution_record')
      ->condition('field_source_link', $this->link)
      ->range(0, 1)
      ->execute();
    if (!empty($id)) {
      if ($return_node) {
        $id = reset($id);
        return $this->entityTypeManager->getStorage('node')->load($id);
      }

      return TRUE;
    }

    return FALSE;
  }

  /**
   * Get the entity linked to this source link.
   *
   * @return \Drupal\node\NodeInterface|null
   *   The linked entity or false if not found.
   */
  public function getLinkedEntity(): ?NodeInterface {
    $entity = $this->isDuplicated(TRUE);
    return ($entity instanceof NodeInterface) ? $entity : NULL;
  }

  /**
   * Retrieves an organization node by its title.
   *
   * @param string[] $organizations
   *   Title of the organization.
   *
   * @return \Drupal\node\NodeInterface[]|null
   *   Organization if found, null otherwise.
   */
  protected function getOrganizations(array $organizations): ?array {
    $ids = $this->entityTypeManager->getStorage('node')->getQuery()
      ->accessCheck(FALSE)
      ->condition('status', NodeInterface::PUBLISHED)
      ->condition('type', 'organization')
      ->condition('title', $organizations, 'IN')
      ->execute();
    if (!empty($ids)) {
      return $this->entityTypeManager->getStorage('node')->loadMultiple($ids);
    }

    return NULL;
  }

  /**
   * Sync a contribution record from the current link.
   *
   * @param \Drupal\node\NodeInterface|null $entity
   *   Entity to sync.
   * @param bool $save
   *   Save the entity.
   *
   * @return \Drupal\node\NodeInterface
   *   Updated created contribution record.
   */
  public function syncContribRecord(?NodeInterface $entity = NULL, bool $save = TRUE): NodeInterface {
    $entity = is_null($entity) ? $this->getLinkedEntity() : $entity;
    if (!$entity) {
      // Contributors will be added via hook presave.
      return $this->createContribRecord();
    }

    $project = $this->getProject();
    $project_name = ($project && !empty($project->get('field_project_machine_name')->getValue())) ?
      $project->get('field_project_machine_name')->getValue()[0]['value'] :
      NULL;
    $entity->set('field_project_name', $project_name);
    $entity->set('field_draft', !$this->isClosed());
    $entity->set('title', $this->getTitle());
    $entity->set('field_last_status_change', $this->getLastStatusChanged());
    $entity->setChangedTime($this->getChanged());
    $entity->setCreatedTime($this->getCreated());
    $this->addContributors($entity, FALSE);
    if ($save) {
      $entity->save();
    }

    return $entity;
  }

  /**
   * Create a contribution record from the current link.
   *
   * @return \Drupal\node\NodeInterface
   *   Newly created contribution record.
   */
  public function createContribRecord(): NodeInterface {
    $project = $this->getProject();
    $project_name = ($project && !empty($project->get('field_project_machine_name')->getValue())) ?
      $project->get('field_project_machine_name')->getValue()[0]['value'] :
      NULL;
    $contrib_record = $this->entityTypeManager->getStorage('node')->create([
      'type' => 'contribution_record',
      'title' => $this->getTitle(),
      'field_source_link' => $this->getLink(),
      'field_project_name' => $project_name,
      'field_draft' => !$this->isClosed(),
      'created' => $this->getCreated(),
      'changed' => $this->getChanged(),
      'field_last_status_change' => $this->getLastStatusChanged(),
    ]);
    $contrib_record->save();

    return $contrib_record;
  }

  /**
   * Add contributors to the entity from the source.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity object to save contributors to.
   * @param bool $save
   *   Whether to save the changes made or not.
   */
  public function addContributors(EntityInterface $entity, bool $save = TRUE) {
    $current_contributors = $this->getCurrentContributors($entity);

    // Clear the cache of contributors.
    $this->cache->delete('contribution_records:contributors:0:' . $this->getLink());
    $this->cache->delete('contribution_records:contributors:1:' . $this->getLink());

    // Extract the contributors from the source and add them to the entity if
    // they're not there already.
    $source_contributors = $this->getContributors();
    foreach ($source_contributors as $source_contributor) {
      if ($source_contributor['user'] && !in_array($source_contributor['user']->id(), $current_contributors)) {
        // Create the paragraph array.
        $paragraph_info = [
          'type' => 'contributor',
          'field_contributor_user' => ['target_id' => $source_contributor['user']->id()],
          'field_contributor_volunteer' => $source_contributor['volunteer'],
          'field_credit_this_contributor' => $source_contributor['credit'],
          'field_contributor_attribute_orgs' => FALSE,
        ];
        if (!empty($source_contributor['organizations'])) {
          $paragraph_info['field_contributor_attribute_orgs'] = TRUE;
          $paragraph_info['field_contributor_organisation'] = [];
          foreach ($source_contributor['organizations'] as $organization) {
            $paragraph_info['field_contributor_organisation'][] = ['target_id' => $organization->id()];
          }
        }
        if (!empty($source_contributor['customers'])) {
          $paragraph_info['field_contributor_attribute_orgs'] = TRUE;
          $paragraph_info['field_contributor_customer'] = [];
          foreach ($source_contributor['customers'] as $customer) {
            $paragraph_info['field_contributor_customer'][] = ['target_id' => $customer->id()];
          }
        }

        // Save it and append it to the entity.
        $paragraph = $this->entityTypeManager->getStorage('paragraph')->create($paragraph_info);
        $paragraph->save();
        $entity->field_contributors->appendItem($paragraph);
      }
    }

    if ($save) {
      $entity->save();
    }
  }

  /**
   * Adds an individual contributor to a contribution record.
   *
   * @param \Drupal\user\UserInterface $user
   *   User to add.
   * @param \Drupal\Core\Entity\EntityInterface|null $entity
   *   Entity to save the contributor against.
   * @param array $defaults
   *   Default values to add.
   * @param bool $save
   *   Whether to save the changes made or not.
   */
  public function addContributor(UserInterface $user, ?EntityInterface $entity = NULL, array $defaults = [], bool $save = TRUE) {
    $entity = $entity ?? $this->getLinkedEntity();
    $current_contributors = $this->getCurrentContributors($entity);
    if (!in_array($user->id(), $current_contributors)) {
      // Clear the cache of contributors.
      $this->cache->delete('contribution_records:contributors:0:' . $this->getLink());
      $this->cache->delete('contribution_records:contributors:1:' . $this->getLink());

      // Create the paragraph array.
      $paragraph_info = [
        'type' => 'contributor',
        'field_contributor_user' => ['target_id' => $user->id()],
        'field_contributor_attribute_orgs' => FALSE,
      ];

      // Defaults provided?
      if (!empty($defaults)) {
        if (!empty($defaults['volunteer'])) {
          $paragraph_info['field_contributor_volunteer'] = $defaults['volunteer'];
        }
        if (!empty($defaults['attribute_orgs'])) {
          $paragraph_info['field_contributor_attribute_orgs'] = $defaults['attribute_orgs'];
        }
        if (!empty($defaults['organizations'])) {
          $paragraph_info['field_contributor_organisation'] = [];
          foreach ($defaults['organizations'] as $organization) {
            $paragraph_info['field_contributor_organisation'][] = ['target_id' => $organization];
          }
        }
        if (!empty($defaults['customers'])) {
          $paragraph_info['field_contributor_customer'] = [];
          foreach ($defaults['customers'] as $customer) {
            $paragraph_info['field_contributor_customer'][] = ['target_id' => $customer];
          }
        }
      }

      // Save it and append it to the entity.
      $paragraph = $this->entityTypeManager->getStorage('paragraph')->create($paragraph_info);
      $paragraph->save();
      $entity->field_contributors->appendItem($paragraph);
    }

    if ($save) {
      $entity->save();
    }
  }

  /**
   * Create a full contrib record from raw data without fetching from source.
   *
   * @param array $data
   *   Data for the contribution record.
   *
   * @return \Drupal\node\NodeInterface|null
   *   Contrib record or null if there was an issue.
   */
  public function createContribRecordFromRawData(array $data): ?NodeInterface {
    // Check at least that the link structure is correct, without fetching it.
    if (!$this->isValid(FALSE)) {
      $contrib_record = NULL;
    }
    else {
      // Basic data first.
      $draft = !empty($data['closed']);
      $contrib_record = $this->entityTypeManager->getStorage('node')->create([
        'type' => 'contribution_record',
        'title' => $data['title'],
        'field_source_link' => $data['url'],
        'field_project_name' => $data['machine_name'] ?? NULL,
        'field_draft' => !$draft,
        // The following will make it skip the pre_save hook where contributors
        // are fetched from the source, but we don't want that as contributors
        // are part of the $data given in the method.
        'revision_log' => self::IMPORT_MESSAGE,
      ]);
      try {
        // And now add all the contributions.
        $usernames = [];
        foreach ($data['credits'] as $credit_info) {
          if (!empty($credit_info['username']) && !in_array($credit_info['username'], $usernames)) {
            $usernames[] = $credit_info['username'];

            // Create the paragraph array.
            $user = $this->userService->getUserByUsername($credit_info['username']);
            if ($user) {
              $paragraph_info = [
                'type' => 'contributor',
                'field_contributor_user' => ['target_id' => $user->id()],
                'field_contributor_volunteer' => $credit_info['volunteer'] ?? FALSE,
                'field_credit_this_contributor' => $credit_info['credit'] ?? FALSE,
                'field_contributor_attribute_orgs' => FALSE,
              ];
              if (!empty($credit_info['organizations'])) {
                $paragraph_info['field_contributor_attribute_orgs'] = TRUE;
                $credit_info['organizations'] = explode(',', $credit_info['organizations']);
                $organizations = $this->getOrganizations($credit_info['organizations']);
                if ($organizations) {
                  $paragraph_info['field_contributor_organisation'] = [];
                  foreach ($organizations as $organization) {
                    $paragraph_info['field_contributor_organisation'][] = ['target_id' => $organization->id()];
                  }
                }
              }
              if (!empty($credit_info['customers'])) {
                $paragraph_info['field_contributor_attribute_orgs'] = TRUE;
                $credit_info['customers'] = explode(',', $credit_info['customers']);
                $customers = $this->getOrganizations($credit_info['customers']);
                if ($customers) {
                  $paragraph_info['field_contributor_customer'] = [];
                  foreach ($customers as $customer) {
                    $paragraph_info['field_contributor_customer'][] = ['target_id' => $customer->id()];
                  }
                }
              }

              // Save it and append it to the entity.
              $paragraph = $this->entityTypeManager->getStorage('paragraph')->create($paragraph_info);
              $paragraph->save();
              $contrib_record->field_contributors->appendItem($paragraph);
            }
          }
        }

        // Keep timestamps if available.
        if (!empty($data['created'])) {
          $contrib_record->setCreatedTime((int) $data['created']);
        }
        if (!empty($data['changed'])) {
          $contrib_record->setChangedTime((int) $data['changed']);
        }
        if (!empty($data['status_last_changed'])) {
          $contrib_record->set('field_last_status_change', (int) $data['status_last_changed']);
        }

        // Save everything.
        $contrib_record->save();
      }
      catch (\Throwable $e) {
        $contrib_record = NULL;
        $this->loggerFactory->get('contribution_records')
          ->error('Error saving contribution record. Error: @error.', [
            '@error' => $e->getMessage(),
          ]);
      }
    }

    return $contrib_record;
  }

  /**
   * Gets a list of current contributors for this entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity to check contributors.
   *
   * @return array
   *   List of contributor user ids.
   */
  protected function getCurrentContributors(EntityInterface $entity): array {
    $current_contributors_paragraphs = $entity->get('field_contributors')->getValue() ?? [];
    $current_contributors = [];
    foreach ($current_contributors_paragraphs as $paragraph) {
      if (!empty($paragraph)) {
        $paragraph = $this->entityTypeManager->getStorage('paragraph')->load($paragraph['target_id']);
        $current_contributors[] = $paragraph->get('field_contributor_user')->getValue()[0]['target_id'] ?? 0;
      }
    }
    return $current_contributors;
  }

  /**
   * Return the type of the source link.
   *
   * @return string
   *   Whether the source link is an issue or a merge request.
   */
  public function getSourceType(): string {
    $type = '';
    // See if it's an issue.
    if ($this->source instanceof GitDrupalCodeIssue) {
      $type = 'issue-gitlab';
    }
    elseif ($this->source instanceof DrupalOrgIssue) {
      $type = 'issue-drupalorg';
    }
    elseif ($this->source instanceof GitDrupalCodeMergeRequest) {
      $type = 'merge-request-gitlab';
    }

    return $type;
  }

}
