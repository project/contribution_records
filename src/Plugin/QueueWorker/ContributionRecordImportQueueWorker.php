<?php

namespace Drupal\contribution_records\Plugin\QueueWorker;

use Drupal\contribution_records\SourceLink;
use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines 'contribution_records_import_queue_worker' queue worker.
 *
 * Run via `drush` like this:
 * `drush queue:run contribution_records_import_queue_worker`.
 *
 * @QueueWorker(
 *   id = "contribution_records_import_queue_worker",
 *   title = @Translation("Import Queue Worker")
 * )
 */
class ContributionRecordImportQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory')->get('contribution_records'),
      $container->get('class_resolver')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    protected LoggerInterface $logger,
    protected ClassResolverInterface $classResolver,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    if (!$this->validateItem($data)) {
      return FALSE;
    }

    /** @var \Drupal\contribution_records\SourceLink $source_link_instance */
    $source_link_instance = $this->classResolver->getInstanceFromDefinition(SourceLink::class);
    $source_link = $source_link_instance->setLink($data['url']);
    if ($source_link->isValid(FALSE)) {
      if (!$source_link->isDuplicated()) {
        $source_link->createContribRecordFromRawData($data);
      }
      else {
        $this->logger->notice($this->t('The item with URL @url is already imported.', [
          '@url' => $data['url'],
        ]));
      }
    }
    else {
      $this->logger->error($this->t('The item with URL @url cannot be imported due to: @error', [
        '@url' => $data['url'],
        '@error' => $source_link->getError(),
      ]));
    }
  }

  /**
   * Validate item array to make sure all key elements are there.
   *
   * @param array $data
   *   Item to validate.
   *
   * @return bool
   *   Whether the item was valid or not.
   */
  protected function validateItem(array $data) {
    if (
      empty($data['url']) ||
      empty($data['title']) ||
      empty($data['credits'])
    ) {
      $this->logger->error($this->t('The queued item did not match the required structure.'));
      return FALSE;
    }

    return TRUE;
  }

}
