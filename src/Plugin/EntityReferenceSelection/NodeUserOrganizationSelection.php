<?php

namespace Drupal\contribution_records\Plugin\EntityReferenceSelection;

use Drupal\node\Plugin\EntityReferenceSelection\NodeSelection;
use Drupal\user\Entity\User;

/**
 * Filter organizations that the user belongs to.
 *
 * @EntityReferenceSelection(
 *   id = "default:node_user_organization",
 *   label = @Translation("User organizations"),
 *   entity_types = {"node"},
 *   group = "default",
 *   weight = 3
 * )
 */
class NodeUserOrganizationSelection extends NodeSelection {

  /**
   * {@inheritdoc}
   */
  protected function buildEntityQuery($match = NULL, $match_operator = 'CONTAINS') {
    $query = parent::buildEntityQuery($match, $match_operator);
    if (!isset($this->configuration['filter'])) {
      return $query;
    }
    $filter_settings = $this->configuration['filter'];
    $user = $filter_settings['user'] ?? NULL;
    if (!$user) {
      return $query;
    }

    $user = User::load($user);
    if (!$user->hasField('field_organizations')) {
      return $query;
    }

    /** @var \Drupal\entity_reference_revisions\EntityReferenceRevisionsFieldItemList $user_organizations */
    $user_organizations = $user->get('field_organizations');
    if ($user_organizations->count() > 0) {
      $filter_organizations = [];
      foreach ($user_organizations->referencedEntities() as $organization) {
        $filter_organizations[] = $organization->get('field_organization_reference')->getValue()[0]['target_id'];
      }
      $query->condition('nid', $filter_organizations, 'IN');
    }

    return $query;
  }

}
